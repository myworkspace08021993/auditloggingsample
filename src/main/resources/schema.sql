CREATE TABLE IF NOT EXISTS users (
    id  bigint NOT NULL,
    user_id  character varying(255),
    created_at bigint ,
    account_id character varying(255),
    first_name character varying(255) NOT NULL,
    last_name character varying(255) ,
    PRIMARY KEY (id)
);


CREATE TABLE IF NOT EXISTS audit ( id bigint NOT NULL, created_at timestamp without time zone NOT NULL, account_id character varying(255) NOT NULL, audit_object_type character varying(255), audit_object_tenant_id character varying(255) ,audit_action_result character varying(255), created_by character varying(255),operation character varying(255),source_ip character varying(255),  data jsonb);
CREATE SEQUENCE IF NOT EXISTS audit_id_seq START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;