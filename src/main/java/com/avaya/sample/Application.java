package com.avaya.sample;

import com.avaya.sample.user.util.UserEntityToModelConverter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;

import com.avaya.sample.user.util.UserModelToEntityConverter;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication(scanBasePackages = {"com.avaya.sample.user", "com.avaya.metam.audit"})
@EnableJpaRepositories(basePackages = {"com.avaya.sample.user", "com.avaya.metam.audit"})
@EntityScan(basePackages = {"com.avaya.sample.user", "com.avaya.metam.audit"})
@EnableSwagger2
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
	@Bean
	public UserModelToEntityConverter modelToEntityConvreter() {
		return new UserModelToEntityConverter();
	}
	
	@Bean
	public UserEntityToModelConverter entityToModelconverter() {
		return new UserEntityToModelConverter();
	}

}
