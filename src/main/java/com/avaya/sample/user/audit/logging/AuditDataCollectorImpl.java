package com.avaya.sample.user.audit.logging;

import com.avaya.metam.audit.model.AuditDataModel;
import com.avaya.metam.audit.model.AuditMethodData;
import com.avaya.metam.audit.service.AuditDataCollector;

import com.avaya.sample.user.model.UserModel;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.sql.Timestamp;
import org.springframework.stereotype.Service;

@Service
public class AuditDataCollectorImpl implements AuditDataCollector {

	@Override
	public AuditDataModel populateAuditData(AuditMethodData methodData) throws Exception {
    AuditDataModel auditDataModel = new AuditDataModel();
    UserModel user = (UserModel) methodData.getArgs()[0];
    ObjectMapper objectMapper = new ObjectMapper();
    try{
      auditDataModel.setData(objectMapper.convertValue(user, JsonNode.class));
    }catch (Exception e){
      System.out.println(e);
    }
    auditDataModel.setAccountId("IDFCG");
    auditDataModel.setOperation("Create");
    auditDataModel.setCreatedBy("Admin");
    auditDataModel.setSourceIp("127.0.0.0");
    auditDataModel.setAuditActionResult("Data Creation");
    auditDataModel.setAuditObjectTenantId("IDFCG");
    auditDataModel.setAuditObjectType("Group");
    auditDataModel.setCreatedAt(new Timestamp(System.currentTimeMillis()));
    return auditDataModel;

	}

}
