package com.avaya.sample.user.util;

import com.avaya.sample.user.model.UserModel;
import org.springframework.core.convert.converter.Converter;

import com.avaya.sample.user.jpa.entity.User;


public class UserEntityToModelConverter implements Converter<User, UserModel> {

	@Override
	public UserModel convert(User userEntity) {
		
		if(userEntity==null) {
			return null;
		}
		
		UserModel userModel = new UserModel();
		userModel.setUserId(userEntity.getUserId());
		userModel.setFirstName(userEntity.getFirstName());
		userModel.setLastName(userEntity.getLastName());
		userModel.setAccountId(userEntity.getAccountId());
		
		return userModel;
	}

}
