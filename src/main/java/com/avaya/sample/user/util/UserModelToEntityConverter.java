package com.avaya.sample.user.util;

import com.avaya.sample.user.jpa.entity.User;
import com.avaya.sample.user.model.UserModel;
import org.springframework.core.convert.converter.Converter;


public class UserModelToEntityConverter implements Converter<UserModel, User> {

	@Override
	public User convert(UserModel userModel) {
		
		if(userModel==null) {
			return null;
		}
		
		User userEntity = new User();
		userEntity.setUserId(userModel.getUserId());
		userEntity.setFirstName(userModel.getFirstName());
		userEntity.setLastName(userModel.getLastName());
		userEntity.setCreatedAt(System.currentTimeMillis());
		userEntity.setAccountId(userModel.getAccountId());
		
		return userEntity;
	}

}
