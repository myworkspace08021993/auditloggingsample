package com.avaya.sample.user.service;

import com.avaya.sample.user.model.UserModel;


public interface UserService {
	
	UserModel addUser(UserModel user);
}
