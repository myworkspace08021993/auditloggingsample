package com.avaya.sample.user.service;

import com.avaya.sample.user.model.UserModel;
import com.avaya.sample.user.jpa.entity.User;
import com.avaya.sample.user.util.UserEntityToModelConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.avaya.sample.user.jpa.repository.UserJpaRepository;
import com.avaya.sample.user.util.UserModelToEntityConverter;


@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	UserJpaRepository repo;
	
	@Autowired
	UserModelToEntityConverter modelToEntityconverter;
	
	@Autowired
  UserEntityToModelConverter entityToModelConverter;
	
	@Override
	public UserModel addUser(UserModel user) {
		
		User userEntity = modelToEntityconverter.convert(user);
		
		repo.save(userEntity);
		return user;
	}
	
}
