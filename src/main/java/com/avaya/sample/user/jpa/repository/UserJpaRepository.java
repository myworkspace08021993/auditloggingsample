package com.avaya.sample.user.jpa.repository;

import com.avaya.sample.user.jpa.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface UserJpaRepository extends JpaRepository<User, Long> {
	
}
