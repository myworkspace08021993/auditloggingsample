package com.avaya.sample.user.controller;

import com.avaya.sample.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.avaya.sample.user.model.UserModel;


@RestController
public class UserController {

	@Autowired
  UserService service;
	
	/*
	 * @GetMapping(value = "/users/{id}", produces =
	 * MediaType.APPLICATION_JSON_VALUE ) public ResponseEntity<User>
	 * getUser(@PathVariable("id") String id){ User user = service.getUser(id);
	 * 
	 * return ResponseEntity.status(HttpStatus.OK).body(user); }
	 * 
	 * @GetMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE )
	 * public ResponseEntity<List<User>> getUser(){ List<User> users =
	 * service.getAllUsers();
	 * 
	 * return ResponseEntity.status(HttpStatus.OK).body(users); }
	 */

	@PostMapping(value = "/users", consumes = MediaType.APPLICATION_JSON_VALUE, 
	        produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<UserModel> addUser(@RequestBody UserModel user) {
		UserModel result = service.addUser(user);

		return ResponseEntity.status(HttpStatus.ACCEPTED).body(result);
	}
	

}
